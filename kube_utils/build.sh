#!/usr/bin/env bash

################################################################################
#
#   Builds a new Docker Image
#   Notes:
#           optionally takes named params;
#          --name: optionally pass image name to docker
#                  container;
#          --tag: docker image tag
#                  by default: latest
#          --release: optionally pass in 'true' to push it to AWS ECR Image
#                     repository.
#                     by default: false
#          --account: AWS Account ID
#                  by default: <goldeneye account ID>
#
#   Usage:
#          bash build.sh --release=true
#
#
################################################################################

set -e

while [ $# -gt 0 ]; do
  case "$1" in
    --name=*)
      name="${1#*=}"
      ;;
    --account=*)
      account="${1#*=}"
      ;;
    --region=*)
      region="${1#*=}"
      ;;
    --tag=*)
      tag="${1#*=}"
      ;;
    --release=*)
      release="${1#*=}"
      ;;
    --update_model=*)
      update_model="${1#*=}"
      ;;

    *)
      printf "***************************\n"
      printf "* Error: Invalid argument.*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
done

# Clone the repository to provide the code to make predictions
REPOSITORY_WEBAPP_SSH=git@bitbucket.org:geanalytics/webapp-backend-sagittarius-a.git -b feature/QUAN-194-model-prediction
# Clone the repository to provide the classes
REPOSITORY_DATASCIENCE_SSH=git@bitbucket.org:geanalytics/datascience-quantitative-modeling.git -b model_deployment

IMAGE_NAME=${name:-idme}
IMAGE_TAG=${tag:-latest}
AWS_ACCOUNT_ID=${account:-288762805072}
AWS_DEFAULT_REGION=${region:-eu-west-1}
RELEASE=${release:-false}
UPDATE_MODEL=${update_model:-true}

rm -rf src/utils/datascience
git clone $REPOSITORY_DATASCIENCE_SSH ./src/utils/datascience
#rm -rf src/utils/webapp
#git clone $REPOSITORY_WEBAPP_SSH ./src/utils/webapp

if [ ${UPDATE_MODEL} == true ]; then
    #rm -rf src/model_files
    echo "Downloading latest model files..."
    aws s3 cp s3://prd-science-data-quant-encrypted/ src/utils/datascience/MachineCreditScoring/Project_Code --recursive
    echo "Finished downloading latest model files"
fi

#rm -rf src/utils/code
#echo "Clone bitbucket repository datascience-quantitative-modeling"
#git clone $REPOSITORY_WEBAPP_SSH ./src/utils/code/webapp
#git clone $REPOSITORY_DATASCIENCE_SSH ./src/utils/code/datascience

echo "Building docker image '${IMAGE_NAME}' with ENV vars DB_HOST $DB_HOST, DB_NAME $DB_NAME ..."
docker build --tag ${IMAGE_NAME}:$IMAGE_TAG --build-arg DB_HOST=$DB_HOST --build-arg DB_PORT=$DB_PORT --build-arg DB_NAME=$DB_NAME --build-arg DB_USER=$DB_USER --build-arg DB_PWD=$DB_PWD .

echo "Finished building local image. Verifying image was added by listing images on cache containing '${IMAGE_NAME}'"
docker images | grep ${IMAGE_NAME}


if [ ${RELEASE} == true ]; then

  echo "Logging into AWS ECR..."
  loggin=$(aws ecr get-login --region ${AWS_DEFAULT_REGION} --no-include-email)
  ${loggin}

  # note: try-catch in case it already exists
  aws ecr create-repository --repository-name ${IMAGE_NAME} --region ${AWS_DEFAULT_REGION} || echo 'AWS ECR Repository exists: ' && echo '' && aws ecr describe-repositories

    echo "Tagging image..."
    docker tag $IMAGE_NAME:$IMAGE_TAG $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_NAME:$IMAGE_TAG

    echo "Pushing into AWS ECR image repository"
    docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_NAME:$IMAGE_TAG

fi

echo "Finished. Bye!"