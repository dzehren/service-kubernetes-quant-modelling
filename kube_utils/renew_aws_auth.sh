#!/usr/bin/env bash

while [ $# -gt 0 ]; do
  case "$1" in
    --account=*)
      account="${1#*=}"
      ;;
    --region=*)
      region="${1#*=}"
      ;;

    *)
      printf "***************************\n"
      printf "* Error: Invalid argument.*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
done



AWS_ACCOUNT=${account:-288762805072}
AWS_REGION=${region:-eu-west-1}

DOCKER_REGISTRY_SERVER=https://${AWS_ACCOUNT}.dkr.ecr.${AWS_REGION}.amazonaws.com
DOCKER_USER=AWS
DOCKER_PASSWORD=`aws ecr get-login --region ${AWS_REGION} --registry-ids ${AWS_ACCOUNT} | cut -d' ' -f6`
kubectl delete secret aws-registry || echo ""
kubectl create secret docker-registry aws-registry \
    --docker-server=$DOCKER_REGISTRY_SERVER \
    --docker-username=$DOCKER_USER \
    --docker-password=$DOCKER_PASSWORD \
    --docker-email=no@email.local


kubectl patch serviceaccount default -p '{"imagePullSecrets":[{"name":"aws-registry"}]}'
echo "Finished refreshing AUTH credentials"