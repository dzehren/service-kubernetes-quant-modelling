FROM continuumio/miniconda3:4.5.11

# Update aptitude with new repo
RUN apt-get update

# Setup Environment
RUN conda create -n goldeneye python=3.6
RUN apt-get install --reinstall build-essential -y
RUN apt-get install gcc -y

# Install requirements; firstly, copy the requirements-file to root dirctory
COPY conda_requirements.txt ./
RUN /bin/bash -c "source activate goldeneye && conda install -c conda-forge --file conda_requirements.txt -y && source deactivate"
COPY requirements.txt ./
RUN /bin/bash -c "source activate goldeneye && pip install -r requirements.txt && source deactivate"

RUN pip install --upgrade awscli

ENV PATH /opt/conda/envs/goldeneye/bin:$PATH

# Copy App Data
RUN mkdir app_data
COPY src /app_data

RUN echo DB_HOST: $DB_HOST, DB_PORT: $DB_PORT, DB_NAME $DB_NAME

COPY entrypoint.sh ./
RUN chmod +x entrypoint.sh

# Inform docker that the container listens on the network port 5000
EXPOSE 5000

ENTRYPOINT ["./entrypoint.sh"]
CMD ["$DB_HOST", "$DB_PORT"]