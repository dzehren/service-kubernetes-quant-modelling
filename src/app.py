from datetime import datetime as dt
from typing import List, Dict

from flask import jsonify, request

#from utils.utils import find_companies, get_company_name_id
from utils.datascience.MachineCreditScoring.Project_Code.main_model_prediction import call_class
from langdetect import detect
import logging

APP_NAME = 'quant-modelling-microservice'


def _get_timestamp():
    return dt.now().strftime("%Y-%m-%d %H:%M:%S")


def health_status():
    return jsonify({
        'app': APP_NAME,
        'health': 'good',
        'processed_at': _get_timestamp(),
    })

def company_lookup():
    body: Dict = request.json
    response: List = []
    #for company in body["company_synonymes"]:
    #    response.append(get_company_name_id(company))
    response.append(call_class)
    return jsonify({
        'app': APP_NAME,
        'response': response,
        'processed_at': _get_timestamp(),
    })
