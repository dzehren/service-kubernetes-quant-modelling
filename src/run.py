from config import connex_app
import logging


app = connex_app
# Read the swagger.yml file to configure the endpoints
app.add_api('swagger.yml')

if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.info("Starting Flask app")
    app.run(host='0.0.0.0', port=5000, debug=True)
