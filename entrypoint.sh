#!/usr/bin/env bash

source activate goldeneye

#source ~/.bashrc
echo DB_HOST: $DB_HOST, DB_PORT: $DB_PORT, DB_NAME: $DB_NAME

#python app_data/run.py
#cd app_data && gunicorn --log-level debug --worker-class eventlet --config /app_data/gunicorn_config.py run:connex_app
#cd app_data && gunicorn --log-level debug --worker-class gevent --config /app_data/gunicorn_config.py run:connex_app

# run gunicorn APP_MODULE of the pattern $(MODULE_NAME):$(VARIABLE_NAME) with the configuration in 'gunicorn_config.py'
cd app_data && gunicorn --config /app_data/gunicorn_config.py run:connex_app